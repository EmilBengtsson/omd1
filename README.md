Fråga 1:
Backend:
Computer - klass
Memory - abstrakt klass
Program - är en lista med kommandon?
Address - klass
LongWord - klass
LongMemory - klass

Kommandon:
Factorial - klass
Mul - klass
Add - klass
Jump - klass
JumpEq - klass
Print - klass
Halt - klass
Copy - klass

Klasser att lägga till:
VeryLongMemory - klass
VeryLongWord - klass
BinOp - abstrakt klass till mul och add
word - abstrakt klass till longword och verylongword
instruction - interface som implementeras av alla operationer

Fråga 2:
En list-klass, t.ex. ArrayList, LinkedList osv...

Fråga 3:
Backend och Kommando-paket. Se ovan (uppgift 1).

Fråga 4:
Det bör användas för alla operationer i programmet. Vi lägger till ett interface, instruction,
som har en "execute"- eller "do"-metod.

Fråga 5:
Att ha en BinOp som hanterar både mul och add för att förhindra duplicerad kod
Liknande kan implementeras för Jump och JumpEq

Fråga 6:
Designmönstert strategy går ut på att man i mitten av exekverinegn av koden kan ändra strategin av
en viss klass genom att överge samma uppgift till en annan klass som inplementerar samma interface.

I detta projekt kan man använda sig av detta mönster för att ändra ansvaret mellan longmemory och 
verlylongmemory samt mellan longword och verylongword.

Fråga 7:
I Add klassen

Fråga 8:
Se bifogad bild