package memory;

public class Address implements Operand {
	
	private int memCell;
	
	public Address(int memCell) {
		this.memCell = memCell;
	}

	public int getIndex() {
		return memCell;
	}
	
	public boolean equals(Operand o, Memory memory){
		return this.getWord(memory).equals(o, memory);
	}
	
	@Override
	public Word getWord(Memory memory) {
		return memory.getWord(memCell);
	}
	
	public String toString(){
		return "["+Integer.toString(memCell)+"]";
	}
	
	public String getString(Memory memory){
		return memory.getWord(memCell).toString();
	}
}
