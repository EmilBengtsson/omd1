package memory;

public class LongMemory extends Memory {

	public LongMemory(int size) {
		super(size);
	}
	
	public Word getWord(int i) {
		return words[i];
	}

	public void changeWord(int index, Word word) {
		words[index] = word;
	}
}