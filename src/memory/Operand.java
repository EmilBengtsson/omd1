package memory;

public interface Operand {
	
    public Word getWord(Memory memory);
    public String getString(Memory memory);
    public boolean equals(Operand o, Memory memory);
}
