package memory;

public abstract class Word implements Operand {
	
	public abstract void mul(Word left, Word right);
	
	public abstract void add(Word left, Word right);
	
	public abstract boolean equals(Operand o, Memory memory);
	
	public abstract Word getWord(Memory memory);

}
