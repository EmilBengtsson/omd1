package memory;

public abstract class Memory {
	protected Word[] words;
	
	public Memory(int size) {
		words = new Word[size];
	}
	
	public abstract Word getWord(int i);
	
	public abstract void changeWord(int index, Word word);
}
