package memory;

public class LongWord extends Word {
	
	private long word;

	public LongWord(long word) {
		super();
		this.word = word;
	}
	
	public void add(Word left, Word right) {
		word = ((LongWord)left).word + ((LongWord)right).word;
	}
	
	public void mul(Word left, Word right) {
		word = ((LongWord)left).word * ((LongWord)right).word;
	}
	
	public boolean equals(Operand o, Memory memory){
		LongWord other = (LongWord)(o.getWord(memory));
		return equals(other);
	}
	
	private boolean equals(LongWord other) {
		return word == other.word;
	}
	
	public Word getWord(Memory memory) {
		return this;
	}
	public String toString(){
		return String.valueOf(word);
	}
	
	public String getString(Memory memory){
		return toString();
	}

}
