package computer;

import memory.Memory;

public interface Instruction {
	
	public void execute(Memory memory, ProgramCounter pC);
	
}
