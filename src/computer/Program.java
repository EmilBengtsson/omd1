package computer;
import java.util.ArrayList;
import memory.Memory;

public abstract class Program {
	private ArrayList<Instruction> instructions;
	private ProgramCounter pC;

	public Program() {
		instructions = new ArrayList<Instruction>();
		pC = new ProgramCounter();
	}
	
	public void add(Instruction i) {
		
		instructions.add(i);
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<instructions.size();i++ ) {
			sb.append(i + instructions.get(i).toString() + "\n");
		}
		return sb.toString();
	}
	
	public void execute(Memory memory) {
		while(pC.getValue()>=0){
			instructions.get(pC.getValue()).execute(memory, pC);
		}
	}
}
