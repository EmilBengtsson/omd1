package computer;

public class ProgramCounter {
	
	private int counter;
	
	public ProgramCounter() {
		counter = 0;
	}
	
	public int getValue() {
		return counter;
	}
	
	public void setValue(int value) {
		counter = value;
	}
	
	public void increment() {
		counter++;
	}
	
	public void terminate() {
		setValue(-1);
	}
}
