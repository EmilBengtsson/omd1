package commands;
import memory.*;

public class Add extends BinOp {	
	public Add(Operand op1, Operand op2, Address addr) {
		super(op1, op2, addr);
	}
	
	protected void operation(Word left, Word right, Word result) {
		result.add(left, right);
	}
	
	public String toString(){
		return " ADD "  + super.toString();
	}
	
	
}
