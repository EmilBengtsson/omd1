package commands;

import computer.Instruction;
import computer.ProgramCounter;
import memory.Memory;

public class Halt implements Instruction {

	public void execute(Memory memory, ProgramCounter pC) {
		pC.terminate();
	}
	public String toString(){
		return " HLT ";
	}

}
