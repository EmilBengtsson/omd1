package commands;

import computer.Instruction;
import computer.ProgramCounter;
import memory.Memory;

public class Jump implements Instruction {

	protected int index;
	
	public Jump(int index) {
		this.index = index;
	}
	
	public void execute(Memory memory, ProgramCounter pC) {
		pC.setValue(index);
	}
	public String toString(){
		return " JMP " + index;
	}
}
