package commands;
import memory.*;

public class Mul extends BinOp{
	public Mul(Operand op1, Operand op2, Address addr) {
		super(op1, op2, addr);
	}
	
	protected void operation(Word left, Word right, Word result) {
		result.mul(left, right);		
	}
	
	public String toString(){
		return " MUL "  + super.toString();
	}


}
