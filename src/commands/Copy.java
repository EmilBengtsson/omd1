package commands;

import computer.Instruction;
import computer.ProgramCounter;
import memory.Address;
import memory.Memory;
import memory.Operand;

public class Copy implements Instruction{
	private Operand o1;
	private Address a;
	
	public Copy(Operand o1, Address a) {
		this.o1 = o1;
		this.a = a;
	}

	public void execute(Memory memory, ProgramCounter pC) {
		memory.changeWord(a.getIndex(), o1.getWord(memory));
		pC.increment();
	}
	
	public String toString(){
		return " CPY "  + o1 + " " + a;
	}
	
	
}
