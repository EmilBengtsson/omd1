package commands;
import computer.Instruction;
import computer.ProgramCounter;
import memory.Memory;
import memory.Operand;

public class Print implements Instruction {
	private Operand o;
	
	public Print(Operand o) {
		this.o = o;
	}

	public void execute(Memory memory, ProgramCounter pC) {
		System.out.println(o.getString(memory));
		pC.increment();
	}
	public String toString(){
		return " PRT "  + o ;
	}

}
