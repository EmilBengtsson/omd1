package commands;
import computer.Instruction;
import computer.ProgramCounter;
import memory.*;

public abstract class BinOp implements Instruction {
	private Operand op1, op2;
	protected Address addr;
	
	public BinOp(Operand o1, Operand o2, Address a) {
		op1 = o1;
		op2 = o2;
		addr = a;
	}
	
	public void execute(Memory memory, ProgramCounter pC) {
		Word left = op1.getWord(memory);
		Word right = op2.getWord(memory);
		Word destination = addr.getWord(memory);
		operation(left, right, destination);
		pC.increment();
	}
	
	protected abstract void operation(Word left, Word right, Word result);
	
	public String toString() {
		return op1 + " " + op2 + " " + addr;
	}
}
