package commands;

import computer.ProgramCounter;
import memory.Memory;
import memory.Operand;

public class JumpEq extends Jump {
	private Operand o1, o2;

	public JumpEq(int value, Operand o1, Operand o2) {
		super(value);
		this.o1 = o1;
		this.o2 = o2;
	}
	
	@Override
	public void execute(Memory memory, ProgramCounter pC) {
		if(o1.equals(o2, memory)) {
			super.execute(memory, pC);
		}
		else{
			pC.increment();
		}
	}
	public String toString(){
		return " JEQ "  + index + " " + o1 + " " + o2 ;
	}

}
